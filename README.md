# Polis Docker Containers

This project publishes Docker containers which are built from snapshots of the
"edge" branch of [Polis](https://github.com/compdemocracy/polis.git).
